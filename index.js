function soNguyenDuongNhoNhat() {
  var soNguyenDuong = 0;
  for (var i = 1; soNguyenDuong < 10000; i++) {
    soNguyenDuong += i;
  }
  document.querySelector(
    ".alert-warning"
  ).innerHTML = `<p class="py-3">Số nguyên dương nhỏ nhất: ${i}</p>`;
}

function tinhTong() {
  var x = document.querySelector("#x").value * 1;
  var n = document.querySelector("#n").value * 1;
  var tong = 0;
  for (var i = 1; i <= n; i++) {
    tong += Math.pow(x, i);
  }

  document.querySelector(
    "#bai_2"
  ).innerHTML = `<p class="py-3">Tổng: ${tong}</p>`;
}

function tinhGiaiThua() {
  var n = document.querySelector("#so-n").value * 1;
  var giaiThua = 1;
  for (var i = 1; i <= n; i++) {
    giaiThua *= i;
  }
  document.querySelector(
    "#bai_3"
  ).innerHTML = `<p class="py-3">Giai thừa: ${giaiThua}</p>`;
}

function taoDiv() {
  var soDiv = document.querySelector("#so-div").value * 1;
  var b4Container = document.querySelector("#b4Container");
  // Xóa nội dung hiện tại của #bgContainer
  b4Container.innerHTML = "";
  // Tạo các thẻ div và thêm vào #bgContainer
  for (var i = 1; i <= soDiv; i++) {
    var div = document.createElement("div");
    if (i % 2 == 0) {
      div.classList.add("bg-warning", "text-white");
      div.innerHTML = `<p>Div chẵn: ${i}</p>`;
    } else {
      div.classList.add("bg-primary", "text-white");
      div.innerHTML = `<p>Div lẻ: ${i}</p>`;
    }

    b4Container.appendChild(div);
  }
  // Set margin thẻ p
  var p = document.querySelectorAll("p");
  for (var j = 0; j < p.length; j++) {
    p[j].style.margin = "0";
  }
}
